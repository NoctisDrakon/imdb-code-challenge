package com.example.imdbmovies.extensions

import com.example.imdbmovies.movies.Movie
import com.example.imdbmovies.util.DateUtil
import java.util.*
import kotlin.collections.ArrayList

fun ArrayList<Movie>.filterMoviesByCurrentYear() {
    var newList: ArrayList<Movie> = ArrayList(this.filter { movie ->
        movie.releaseDate!! != null && movie.releaseDate!!.isNotBlank() &&
        (DateUtil.stringToDate(movie.releaseDate!!).get(Calendar.YEAR) == Calendar.getInstance().get(
            Calendar.YEAR)
        )
    })
    this.clear()
    this.addAll(newList)
}