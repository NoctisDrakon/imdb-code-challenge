package com.example.imdbmovies.extensions

import android.os.SystemClock
import android.view.View

/**
 * This extension will allow us get or set the visibility of a view in an easy way
 * and only using boolean values instead the VISIBLE or GONE View enums.
 */
var View.visible: Boolean
    get() = this.visibility == View.VISIBLE
    set(isVisible) {
        this.visibility = if (isVisible) View.VISIBLE else View.GONE
    }

fun View.throttleClicks(
    windowDuration: Long = 800,
    onClick: View.() -> Unit) {
    setOnClickListener(object : View.OnClickListener {

        var lastClickTime = 0L

        override fun onClick(v: View?) {
            val time = SystemClock.elapsedRealtime()
            if (time - lastClickTime >= windowDuration) {
                lastClickTime = time
                onClick()
            }
        }
    })
}