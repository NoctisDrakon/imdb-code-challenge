package com.example.imdbmovies

import android.os.Bundle
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.setupWithNavController
import com.example.imdbmovies.core.base.BaseActivity
import com.example.imdbmovies.extensions.visible
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    private lateinit var navController: NavController
    private lateinit var drawerLayout: DrawerLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        drawerLayout = findViewById(R.id.drawer_layout)

        val toggle = ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawerLayout.addDrawerListener(toggle)

        toggle.syncState()
        setNavListeners()
    }

    private fun setNavListeners() {
        nav_bar.setupWithNavController(navController)
        nav_bar.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.screen_home -> {
                    navController.navigate(R.id.navigate_from_favorites_to_movies)
                    true
                }
                R.id.screen_favorites -> {
                    navController.navigate(R.id.navigate_from_movies_to_favorites)
                    true
                }
                else -> false
            }
        }

        nav_bar.setOnNavigationItemReselectedListener { false }

        nav_view.setNavigationItemSelectedListener {
            drawerLayout.closeDrawer(GravityCompat.START, true)
            when (it.itemId) {
                R.id.screen_watch_later -> {
                    if (navController.currentDestination?.id != R.id.watchLaterFragment) {
                        navController.navigate(R.id.watchLaterFragment)
                        nav_bar.visible = false
                    }
                    true
                }
                else -> false
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        when (navController.currentDestination?.id) {
            R.id.moviesFragment -> {
                nav_bar.menu.findItem(R.id.screen_home).isChecked = true
                nav_bar.visible = true
            }
            R.id.favoritesFragment -> {
                nav_bar.menu.findItem(R.id.screen_favorites).isChecked = true
                nav_bar.visible = true
            }
        }
    }
}
