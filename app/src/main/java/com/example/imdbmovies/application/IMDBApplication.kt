package com.example.imdbmovies.application

import android.app.Application
import com.example.imdbmovies.BuildConfig
import com.example.imdbmovies.core.dagger.Injector
import com.example.imdbmovies.core.dagger.components.DaggerIMDBComponent
import com.example.imdbmovies.core.dagger.components.IMDBComponent
import com.example.imdbmovies.core.dagger.modules.ApiModule
import com.example.imdbmovies.core.dagger.modules.DatabaseModule
import timber.log.Timber

class IMDBApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        Injector.init(createDaggerComponent())
    }

    private fun createDaggerComponent(): IMDBComponent {
        return DaggerIMDBComponent.builder()
            .apiModule(ApiModule(this))
            .databaseModule(DatabaseModule(this))
            .build()
    }

}