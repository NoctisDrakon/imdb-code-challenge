package com.example.imdbmovies.movies

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.PopupMenu
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.imdbmovies.R
import com.example.imdbmovies.extensions.throttleClicks
import com.example.imdbmovies.util.GlideFactory
import io.reactivex.subjects.PublishSubject
import timber.log.Timber

class MoviesAdapter constructor(
    val context: Context?

) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val VIEW_TYPE_MOVIE = 111
        const val VIEW_TYPE_LOADING = 222
    }

    private var movies = mutableListOf<Movie>()
    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var showingLoading = false

    val favoritesClick = PublishSubject.create<Movie>()
    val watchLaterClick = PublishSubject.create<Movie>()

    fun setMovies(movies: MutableList<Movie>) {
        this.movies = movies
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return movies[position].type
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when (viewType) {
            VIEW_TYPE_MOVIE -> {
                val view = inflater.inflate(R.layout.movie_item, parent, false)
                val poster = view.findViewById<ImageView>(R.id.movie_poster)
                val title = view.findViewById<TextView>(R.id.movie_title)
                val rating = view.findViewById<TextView>(R.id.movie_rating)
                val fav = view.findViewById<ImageView>(R.id.movie_fav)
                val more = view.findViewById<ImageView>(R.id.movie_more)
                return MoviesViewHolder(view, poster, title, rating, fav, more)
            }
            VIEW_TYPE_LOADING -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_loading, parent, false)
                return LoadingViewHolder(view)
            }
        }
        throw UnsupportedOperationException("The type you are attempting to use is not currently supported")
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is MoviesViewHolder -> {
                val movie = movies[position]

                Glide.with(holder.poster)
                    .load(GlideFactory.getPosterUrl(movie.posterPath) ?: R.drawable.img_not_available)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .placeholder(R.drawable.placeholder)
                    .into(holder.poster)

                holder.title.text = movie.title
                holder.rating.text = String.format(context?.getString(R.string.rating_format) ?: "", movie.rating)

                if(movie.favorite == 1){
                    holder.fav.setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.ic_star_fav_added))
                } else {
                    holder.fav.setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.ic_star_fav_no_added))
                }

                holder.fav.throttleClicks {
                    favoritesClick.onNext(movie)
                    if(movie.favorite == 1) {
                        holder.fav.setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.ic_star_fav_no_added))
                        movie.favorite = 0
                        notifyDataSetChanged()
                    } else {
                        holder.fav.setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.ic_star_fav_added))
                        movie.favorite = 1
                        notifyDataSetChanged()
                    }
                }

                holder.poster.throttleClicks {
                    context.startActivity(MovieDetailsActivity.getIntent(context, movie))
                }

                holder.more.throttleClicks {
                    val popupMenu = PopupMenu(context, holder.more)
                    if(movie.watchLater == 0) {
                        popupMenu.inflate(R.menu.movie_menu_add)
                        popupMenu.setOnMenuItemClickListener {
                            when (it.itemId) {
                                R.id.movie_add_to_watch -> {
                                    watchLaterClick.onNext(movie)
                                    movie.watchLater = 1
                                    notifyDataSetChanged()
                                    true
                                }
                                else -> false
                            }
                        }
                    } else {
                        popupMenu.inflate(R.menu.movie_menu_remove)
                        popupMenu.setOnMenuItemClickListener {
                            when (it.itemId) {
                                R.id.movie_remove_from_watch -> {
                                    watchLaterClick.onNext(movie)
                                    movie.watchLater = 0
                                    notifyDataSetChanged()
                                    true
                                }
                                else -> false
                            }
                        }
                    }
                    popupMenu.show()
                }
            }
            is LoadingViewHolder -> {
                //nothing
            }
        }

    }

    fun addLoadingFooter() {
        if (!showingLoading) {
            movies.add(Movie(type = VIEW_TYPE_LOADING))
            showingLoading = true
            notifyDataSetChanged()
        }
    }

    fun clear() {
        movies.clear()
        notifyDataSetChanged()
    }

    fun removeLoadingFooter() {
        movies.removeIf { item -> item.type == VIEW_TYPE_LOADING }
        showingLoading = false
        notifyDataSetChanged()
    }

    fun sortByName() {
        movies.sortBy { it.title }
        notifyDataSetChanged()
    }

    fun sortByDate() {
        movies.sortBy { it.releaseDate }
        notifyDataSetChanged()
    }

    class MoviesViewHolder(
        val view: View,
        val poster: ImageView,
        val title: TextView,
        val rating: TextView,
        val fav: ImageView,
        val more: ImageView
    ) : RecyclerView.ViewHolder(view)

    class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}