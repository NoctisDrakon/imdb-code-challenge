package com.example.imdbmovies.movies

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity(tableName = "table_movies")
data class Movie(
    @Transient
    var type: Int = MoviesAdapter.VIEW_TYPE_MOVIE,
    @SerializedName("vote_count")
    var voteCount: Int? = null,
    @PrimaryKey(autoGenerate = false)
    var id: Int? = null,
    @SerializedName("vote_average")
    var rating: String? = null,
    var title: String? = null,
    @SerializedName("poster_path")
    var posterPath: String? = null,
    var adult: Boolean? = null,
    var overview: String? = null,
    @SerializedName("release_date")
    var releaseDate: String? = null,
    var favorite: Int = 0,
    var watchLater: Int = 0): Serializable