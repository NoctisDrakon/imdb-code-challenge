package com.example.imdbmovies.movies

import android.view.View
import androidx.lifecycle.MutableLiveData
import com.example.imdbmovies.core.base.BaseViewModel
import com.example.imdbmovies.core.dagger.Injector
import com.example.imdbmovies.extensions.applySchedulers
import com.example.imdbmovies.extensions.filterMoviesByCurrentYear
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

open class MoviesViewModel : BaseViewModel() {

    val state: MutableLiveData<Status> = MutableLiveData()
    private var currentPage = 1
    private var totalPages = 0
    private var searchQuery = ""

    var moviesResponse: Response? = null
    var moviesError: Throwable? = null

    var dbAlreadyLoaded = false

    @Inject
    lateinit var repository: MoviesRepository

    init {
        Injector.component().inject(this)
    }

    fun loadMoviesFromDB() {
        if (dbAlreadyLoaded) {
            loadMovies()
        } else {
            addDisposable(repository.getMoviesFromDB()
                .applySchedulers()
                .doOnSubscribe { state.value = Status.LOADING }
                .subscribe({
                    if (it.isEmpty()) {
                        Timber.d("No movies found in local database. Proceeding to load from API")
                        state.value = Status.NO_MOVIES_IN_DB
                        loadMovies()
                    } else {
                        moviesResponse = Response(1, 1, 1, ArrayList(it))
                        currentPage = 0
                        totalPages = 1
                        state.value = Status.SUCCESS
                    }
                    dbAlreadyLoaded = true
                }, {
                    moviesError = it
                    Timber.wtf(moviesError)
                    state.value = Status.ERROR
                })
            )
        }
    }

    fun loadMovies(language: String = DEFAULT_LANGUAGE, loadingMore: Boolean = false) {
        if (!loadingMore) {
            resetValues()
        }

        addDisposable(repository.getMovies(language = language, page = currentPage)
            .delay(1000, TimeUnit.MILLISECONDS) // show the "loading row" for at least this time
            .applySchedulers()
            .doOnSubscribe {
                Timber.d("Loading movies from API")
                if (!loadingMore) state.value = Status.LOADING
            }
            .subscribe({
                if (loadingMore) {

                    totalPages = it.totalPages ?: 0
                    it.results.filterMoviesByCurrentYear()
                    moviesResponse?.results?.addAll(it.results)

                    moviesResponse?.results?.let {
                        saveMoviesToDatabase(it)
                    }

                    if (it.results.size == 0) //There wasn't any movie from this year in this page. Let's try again
                        loadMoreMovies()
                    else
                        state.value = Status.SUCCESS
                } else {
                    totalPages = it.totalPages ?: 0
                    moviesResponse = it
                    moviesResponse?.results?.filterMoviesByCurrentYear()

                    moviesResponse?.results?.let {
                        saveMoviesToDatabase(it)
                    }

                    state.value = Status.SUCCESS
                }

            }, {
                moviesError = it
                state.value = Status.ERROR
                Timber.wtf(it)
            })
        )
    }

    private fun saveMoviesToDatabase(movies: ArrayList<Movie>) {
        addDisposable(
            repository.saveMoviesToDB(movies)
                .applySchedulers()
                .subscribe({
                    Timber.d("Movies saved successfully")
                }, {
                    Timber.d("Error saving movies")
                    Timber.wtf(it)
                })
        )
    }

    fun searchMovies(query: String, loadingMore: Boolean = false) {
        if (!loadingMore) {
            resetValues()
        }
        searchQuery = query
        addDisposable(
            repository.searchMovies(query = searchQuery, page = currentPage)
                .delay(1000, TimeUnit.MILLISECONDS) // show the "loading row" for at least this time
                .applySchedulers()
                .subscribe({
                    if (loadingMore) {
                        it.results.filterMoviesByCurrentYear()
                        saveMoviesToDatabase(it.results)
                        moviesResponse?.results?.addAll(it.results)
                        if (it.results.size == 0) //There wasn't any movie from this year in this page. Let's try again
                            loadMoreMovies()
                        else
                            state.value = Status.SUCCESS
                    } else {
                        totalPages = it.totalPages ?: 0
                        moviesResponse = it
                        moviesResponse?.results?.filterMoviesByCurrentYear()
                        moviesResponse?.results?.let { saveMoviesToDatabase(it) }
                        state.value = Status.SUCCESS
                    }

                }, {
                    moviesError = it
                    state.value = Status.ERROR
                    Timber.wtf(it)
                })
        )
    }

    fun loadMoreMovies() {
        if (currentPage < totalPages) {
            currentPage++
            if (searchQuery.isEmpty()) {
                loadMovies(loadingMore = true)
            } else {
                searchMovies(query = searchQuery, loadingMore = true)
            }
        }
    }

    fun addMovieToFavorites(movie: Movie) {
        addDisposable(repository.addMovieToFavorites(movie)
            .applySchedulers()
            .subscribe({
                state.value = Status.MOVIE_ADDED_TO_FAV
            },{
                Timber.wtf(it)
            }))
    }

    fun removeMovieFromFavorites(movie: Movie) {
        addDisposable(repository.removeMovieFromFavorites(movie)
            .applySchedulers()
            .subscribe({
                state.value = Status.MOVIE_REMOVED_FROM_FAV
            },{
                Timber.wtf(it)
            }))
    }

    fun addMovieToWatchLater(movie: Movie) {
        addDisposable(repository.addMovieToWatchLater(movie)
            .applySchedulers()
            .subscribe({
                state.value = Status.MOVIE_ADDED_TO_WL
            },{
                Timber.wtf(it)
            }))
    }

    fun removeMovieFromWatchLater(movie: Movie) {
        addDisposable(repository.removeMovieFromWatchLater(movie)
            .applySchedulers()
            .subscribe({
                state.value = Status.MOVIE_REMOVED_FROM_WL
            },{
                Timber.wtf(it)
            }))
    }

    fun resetValues() {
        currentPage = 1
        totalPages = 0
        searchQuery = ""
    }

    companion object {
        const val DEFAULT_LANGUAGE = "en-US"
    }

}