package com.example.imdbmovies.movies

import com.example.imdbmovies.core.base.BaseRepository
import com.example.imdbmovies.core.db.MovieDao
import com.example.imdbmovies.extensions.applySchedulers
import io.reactivex.Maybe
import io.reactivex.Single
import timber.log.Timber
import javax.inject.Inject

class MoviesRepository @Inject constructor() : BaseRepository() {

    var movieDao: MovieDao = dataBase.movieDao()

    //Network calls
    fun getMovies(language: String, page: Int): Single<Response> {
        return api.getMovies(language = language, page = page)
            .applySchedulers()
            .doOnError {
                Timber.wtf(it)
            }
    }

    fun searchMovies(query: String, page: Int): Single<Response> {
        return api.searchMovies(query = query, page = page)
            .applySchedulers()
            .doOnError {
                Timber.wtf(it)
            }
    }

    //DB calls
    fun getMoviesFromDB(): Maybe<List<Movie>> {
        return movieDao.getAllMovies()
            .applySchedulers()
            .doOnError {
                Timber.wtf(it)
            }
    }

    fun saveMovieToDB(movie: Movie): Single<Long> {
        return movieDao.insertMovie(movie)
            .applySchedulers()
            .doOnError {
                Timber.wtf(it)
            }

    }

    fun saveMoviesToDB(movies: ArrayList<Movie>): Single<List<Long>> {
        return movieDao.insertAllMovies(movies)
            .applySchedulers()
            .doOnError {
                Timber.wtf(it)
            }
    }

    fun deleteAllMoviesFromDB(): Maybe<Unit> {
        return movieDao.deleteAllMovies()
            .applySchedulers()
            .doOnError {
                Timber.wtf(it)
            }
    }

    //Favorites
    fun getFavorites(): Maybe<List<Movie>> {
        return movieDao.getFavorites()
            .applySchedulers()
            .doOnError {
                Timber.wtf(it)
            }
    }

    fun addMovieToFavorites(movie: Movie): Maybe<Unit> {
        return movieDao.addMovieToFavorites(movie.id ?: -1)
            .applySchedulers()
            .doOnError {
                Timber.wtf(it)
            }
    }

    fun removeMovieFromFavorites(movie: Movie): Maybe<Unit> {
        return movieDao.removeMovieFromFavorites(movie.id ?: -1)
            .applySchedulers()
            .doOnError {
                Timber.wtf(it)
            }
    }

    //Watch later
    fun getWatchLater(): Maybe<List<Movie>> {
        return movieDao.getWatchLater()
            .applySchedulers()
            .doOnError {
                Timber.wtf(it)
            }
    }

    fun addMovieToWatchLater(movie: Movie): Maybe<Unit> {
        return movieDao.addMovieToWatchLater(movie.id ?: -1)
            .applySchedulers()
            .doOnError {
                Timber.wtf(it)
            }
    }

    fun removeMovieFromWatchLater(movie: Movie): Maybe<Unit> {
        return movieDao.removeMovieFromWatchLater(movie.id ?: -1)
            .applySchedulers()
            .doOnError {
                Timber.wtf(it)
            }
    }
}