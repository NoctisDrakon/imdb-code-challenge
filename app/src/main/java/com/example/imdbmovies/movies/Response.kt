package com.example.imdbmovies.movies

import com.google.gson.annotations.SerializedName

data class Response(
    val page: Int?,
    @SerializedName("total_results")
    val totalResults: Int?,
    @SerializedName("total_pages")
    val totalPages: Int?,
    var results: ArrayList<Movie>)