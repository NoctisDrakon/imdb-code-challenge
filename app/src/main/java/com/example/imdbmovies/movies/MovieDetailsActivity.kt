package com.example.imdbmovies.movies

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.example.imdbmovies.R
import com.example.imdbmovies.core.base.BaseActivity
import com.example.imdbmovies.util.GlideFactory
import kotlinx.android.synthetic.main.activity_movie_details.*

class MovieDetailsActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_details)

        intent.apply {
            (getSerializableExtra(MOVIE_KEY) as Movie).also {
                Glide.with(this@MovieDetailsActivity)
                    .load(GlideFactory.getPosterUrl(it.posterPath) ?: R.drawable.img_not_available)
                    .centerCrop()
                    .placeholder(R.drawable.placeholder)
                    .into(movie_details_poster)

                movie_details_title.text = it.title
                movie_details_rate.text = String.format(getString(R.string.rating_format), it.rating)
                movie_details_date.text = String.format(getString(R.string.release_date_format), it.releaseDate)
                movie_details_synopsis.text = it.overview

                if(it.adult == false) {
                    movie_details_adult.setTextColor(ContextCompat.getColor(this@MovieDetailsActivity, R.color.colorPrimary))
                    movie_details_adult.text = getString(R.string.movie_for_everyone)
                } else {
                    movie_details_adult.setTextColor(ContextCompat.getColor(this@MovieDetailsActivity, android.R.color.holo_red_dark))
                    movie_details_adult.text = getString(R.string.movie_for_adults)
                }
            }
        }
    }

    companion object {
        const val MOVIE_KEY = "movie_key"
        fun getIntent(context: Context, movie: Movie): Intent {
            return Intent(context, MovieDetailsActivity::class.java).also {
                it.putExtra(MOVIE_KEY, movie)
            }
        }
    }
}
