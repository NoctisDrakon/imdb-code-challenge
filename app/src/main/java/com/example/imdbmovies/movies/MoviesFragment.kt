package com.example.imdbmovies.movies

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.PopupMenu
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.imdbmovies.R

import com.example.imdbmovies.core.base.BaseFragment
import com.example.imdbmovies.core.base.BaseViewModel
import com.example.imdbmovies.extensions.applySchedulers
import com.mancj.materialsearchbar.MaterialSearchBar
import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import kotlinx.android.synthetic.main.fragment_movies.*
import retrofit2.HttpException
import timber.log.Timber
import java.util.concurrent.TimeUnit


class MoviesFragment : BaseFragment() {

    private lateinit var vm: MoviesViewModel
    private lateinit var adapter: MoviesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm = MoviesViewModel()
        adapter = MoviesAdapter(context)
        setVMStateSubscriptions()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_movies, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setRecyclerView()
        setSearchBar()

        menu_sort.setOnClickListener {
            val popupMenu = PopupMenu(context!!, menu_sort)
            popupMenu.inflate(R.menu.menu_sort)
            popupMenu.setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.sort_by_name_descending -> {
                        adapter.sortByName()
                        true
                    }
                    R.id.sort_by_date_ascending -> {
                        adapter.sortByDate()
                        true
                    }
                    else -> false
                }
            }
            popupMenu.show()
        }
        vm.dbAlreadyLoaded = false
        vm.loadMoviesFromDB()
    }

    private fun setSearchBar() {
        searchBar.setOnSearchActionListener(object : MaterialSearchBar.OnSearchActionListener {
            override fun onButtonClicked(buttonCode: Int) {}

            override fun onSearchStateChanged(enabled: Boolean) {}

            override fun onSearchConfirmed(text: CharSequence?) {
                if (text?.isEmpty() == true) {
                    vm.dbAlreadyLoaded = false
                    vm.loadMoviesFromDB()
                } else {
                    vm.searchMovies(text?.toString() ?: "")
                }
            }

        })

        addDisposable(Observable.create(ObservableOnSubscribe<String> {
            searchBar.addTextChangeListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable) {
                    it.onNext(s.toString())
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            })

        }).debounce(250, TimeUnit.MILLISECONDS)
            .applySchedulers()
            .subscribe {
                adapter.clear()
                if (it.isEmpty()) {
                    vm.dbAlreadyLoaded = false
                    vm.loadMoviesFromDB()
                } else {
                    vm.searchMovies(it)
                }
            })
    }

    private fun setRecyclerView() {
        moviesContainer.layoutManager = GridLayoutManager(context, resources.getInteger(R.integer.columns))
        moviesContainer.adapter = adapter
        moviesContainer.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    val visibleThreshold = 2
                    val layoutManager = moviesContainer.layoutManager as GridLayoutManager
                    val lastItem = layoutManager.findLastCompletelyVisibleItemPosition()
                    val currentTotalCount = layoutManager.itemCount
                    if (currentTotalCount <= lastItem + visibleThreshold) {
                        if (vm.state.value != BaseViewModel.Status.LOADING) {
                            adapter.addLoadingFooter()
                            vm.loadMoreMovies()
                        }
                    }
                }
            }
        })

        addDisposable(adapter.favoritesClick.subscribe {
            if (it.favorite == 1) {
                vm.removeMovieFromFavorites(it)
            } else {
                vm.addMovieToFavorites(it)
            }
        })

        addDisposable(adapter.watchLaterClick.subscribe {
            if (it.watchLater == 1) {
                vm.removeMovieFromWatchLater(it)
            } else {
                vm.addMovieToWatchLater(it)
            }
        })
    }

    private fun setVMStateSubscriptions() {
        vm.state.observe(this, Observer {
            when (it) {
                BaseViewModel.Status.LOADING -> {
                    showLoading(true)
                }
                BaseViewModel.Status.SUCCESS -> {
                    showLoading(false)
                    adapter.removeLoadingFooter()
                    vm.moviesResponse?.let { response ->
                        adapter.setMovies(response.results)
                    }
                }
                BaseViewModel.Status.ERROR -> {
                    vm.moviesError?.let {
                        if (it is HttpException) {
                            if (it.response()?.errorBody()?.string().isNullOrEmpty()) {
                                Toast.makeText(context, getString(R.string.error_no_internet), Toast.LENGTH_LONG).show()
                            } else {
                                Toast.makeText(context, it.response()?.errorBody()?.string(), Toast.LENGTH_LONG).show()
                            }
                        } else {
                            Toast.makeText(context, getString(R.string.error_generic), Toast.LENGTH_LONG).show()
                        }
                        adapter.removeLoadingFooter()
                        showLoading(false)
                    }
                }
                BaseViewModel.Status.NO_MOVIES_IN_DB -> {
                    Toast.makeText(context, getString(R.string.no_movies_in_db), Toast.LENGTH_SHORT).show()
                }
                BaseViewModel.Status.MOVIE_ADDED_TO_FAV -> {
                    Toast.makeText(context, getString(R.string.movie_added_to_fav), Toast.LENGTH_SHORT).show()
                }
                BaseViewModel.Status.MOVIE_REMOVED_FROM_FAV -> {
                    Toast.makeText(context, getString(R.string.movie_removed_from_fav), Toast.LENGTH_SHORT).show()
                }
                BaseViewModel.Status.MOVIE_ADDED_TO_WL -> {
                    Toast.makeText(context, getString(R.string.movie_added_to_wl), Toast.LENGTH_SHORT).show()
                }
                BaseViewModel.Status.MOVIE_REMOVED_FROM_WL -> {
                    Toast.makeText(context, getString(R.string.movie_removed_from_wl), Toast.LENGTH_SHORT).show()
                }
            }
        })
    }
}
