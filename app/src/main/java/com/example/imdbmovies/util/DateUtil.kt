package com.example.imdbmovies.util

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class DateUtil {

    companion object {
        private val format: DateFormat = SimpleDateFormat("yyyy-mm-dd", Locale.ENGLISH)

        fun stringToDate(date: String): Calendar {
            val date = format.parse(date)
            val calendar = Calendar.getInstance()
            calendar.time = date
            return calendar
        }
    }

}