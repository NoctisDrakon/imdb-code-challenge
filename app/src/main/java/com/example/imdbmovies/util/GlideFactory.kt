package com.example.imdbmovies.util

import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import javax.inject.Inject

class GlideFactory
@Inject constructor() {

    fun getRequestManager(fragment: Fragment): RequestManager {
        return Glide.with(fragment)
    }

    companion object {
        fun getPosterUrl(path: String?): String? {
            path?.let {
                return "https://image.tmdb.org/t/p/w500/$path"
            }
            return null
        }
    }
}
