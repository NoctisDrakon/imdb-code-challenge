package com.example.imdbmovies.favorites


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager

import com.example.imdbmovies.R
import com.example.imdbmovies.core.base.BaseFragment
import com.example.imdbmovies.core.base.BaseViewModel
import com.example.imdbmovies.movies.MoviesAdapter
import kotlinx.android.synthetic.main.fragment_favorites.*
import timber.log.Timber


class FavoritesFragment : BaseFragment() {

    private lateinit var vm: FavoritesViewModel
    private lateinit var adapter: MoviesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm = FavoritesViewModel()
        adapter = MoviesAdapter(context)
        setVMStateSubscriptions()

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_favorites, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setRecyclerView()
        vm.loadFavorites()
    }

    private fun setRecyclerView() {
        favorites_container.layoutManager = GridLayoutManager(context, resources.getInteger(R.integer.columns))
        favorites_container.adapter = adapter

        addDisposable(adapter.favoritesClick.subscribe {
            if(it.favorite == 1) {
                vm.removeMovieFromFavorites(it)
                vm.loadFavorites()
            } else {
                vm.addMovieToFavorites(it)
                vm.loadFavorites()
            }
        })

        addDisposable(adapter.watchLaterClick.subscribe{
            if(it.watchLater == 1) {
                vm.removeMovieFromWatchLater(it)
            } else {
                vm.addMovieToWatchLater(it)
            }
        })
    }

    private fun setVMStateSubscriptions() {
        vm.state.observe(this, Observer {
            when(it) {
                BaseViewModel.Status.LOADING -> {
                    showLoading(true)
                }
                BaseViewModel.Status.SUCCESS -> {
                    showLoading(false)
                    vm.response?.let { movies ->
                        adapter.setMovies(ArrayList(movies))
                    }
                }
                BaseViewModel.Status.ERROR -> {
                    showLoading(false)
                }
                BaseViewModel.Status.MOVIE_ADDED_TO_FAV -> {
                    Toast.makeText(context, getString(R.string.movie_added_to_fav), Toast.LENGTH_SHORT).show()
                }
                BaseViewModel.Status.MOVIE_REMOVED_FROM_FAV -> {
                    Toast.makeText(context, getString(R.string.movie_removed_from_fav), Toast.LENGTH_SHORT).show()
                }
                BaseViewModel.Status.MOVIE_ADDED_TO_WL -> {
                    Toast.makeText(context, getString(R.string.movie_added_to_wl), Toast.LENGTH_SHORT).show()
                }
                BaseViewModel.Status.MOVIE_REMOVED_FROM_WL -> {
                    Toast.makeText(context, getString(R.string.movie_removed_from_wl), Toast.LENGTH_SHORT).show()
                }
            }
        })
    }
}
