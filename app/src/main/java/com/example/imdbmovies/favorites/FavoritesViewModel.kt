package com.example.imdbmovies.favorites

import androidx.lifecycle.MutableLiveData
import com.example.imdbmovies.core.base.BaseViewModel
import com.example.imdbmovies.core.dagger.Injector
import com.example.imdbmovies.extensions.applySchedulers
import com.example.imdbmovies.movies.Movie
import com.example.imdbmovies.movies.MoviesRepository
import timber.log.Timber
import javax.inject.Inject

class FavoritesViewModel : BaseViewModel() {

    @Inject
    lateinit var repository: MoviesRepository

    val state: MutableLiveData<Status> = MutableLiveData()

    var response: List<Movie>? = null
    var error: Throwable? = null

    init {
        Injector.component().inject(this)
    }

    fun loadFavorites(){
        addDisposable(repository.getFavorites()
            .applySchedulers()
            .doOnSubscribe{ state.value = Status.LOADING }
            .subscribe({
                response = it
                state.value = Status.SUCCESS
            },{
                error = it
                state.value = Status.ERROR
            }))
    }

    fun addMovieToFavorites(movie: Movie) {
        addDisposable(repository.addMovieToFavorites(movie)
            .applySchedulers()
            .subscribe({
                state.value = Status.MOVIE_ADDED_TO_FAV
            },{
                Timber.wtf(it)
            }))
    }

    fun removeMovieFromFavorites(movie: Movie) {
        addDisposable(repository.removeMovieFromFavorites(movie)
            .applySchedulers()
            .subscribe({
                state.value = Status.MOVIE_REMOVED_FROM_FAV
            },{
                Timber.wtf(it)
            }))
    }

    fun addMovieToWatchLater(movie: Movie) {
        addDisposable(repository.addMovieToWatchLater(movie)
            .applySchedulers()
            .subscribe({
                state.value = Status.MOVIE_ADDED_TO_WL
            },{
                Timber.wtf(it)
            }))
    }

    fun removeMovieFromWatchLater(movie: Movie) {
        addDisposable(repository.removeMovieFromWatchLater(movie)
            .applySchedulers()
            .subscribe({
                state.value = Status.MOVIE_REMOVED_FROM_WL
            },{
                Timber.wtf(it)
            }))
    }
}