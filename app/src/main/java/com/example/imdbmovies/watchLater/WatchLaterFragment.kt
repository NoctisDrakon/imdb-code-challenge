package com.example.imdbmovies.watchLater


import android.os.Bundle
import android.widget.Toast
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.example.imdbmovies.R
import com.example.imdbmovies.core.base.BaseFragment
import com.example.imdbmovies.core.base.BaseViewModel
import com.example.imdbmovies.movies.MoviesAdapter
import kotlinx.android.synthetic.main.fragment_watch_later.*
import timber.log.Timber


class WatchLaterFragment : BaseFragment() {

    private lateinit var vm: WatchLaterViewModel
    private lateinit var adapter: MoviesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm = WatchLaterViewModel()
        adapter = MoviesAdapter(context)
        setVMStateSubscriptions()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_watch_later, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setRecyclerView()
        vm.loadWatchLaterMovies()
    }

    private fun setRecyclerView() {
        watch_later_container.layoutManager = GridLayoutManager(context, resources.getInteger(R.integer.columns))
        watch_later_container.adapter = adapter

        addDisposable(adapter.favoritesClick.subscribe {
            if(it.favorite == 1) {
                vm.removeMovieFromFavorites(it)
            } else {
                vm.addMovieToFavorites(it)
            }
        })

        addDisposable(adapter.watchLaterClick.subscribe{
            if(it.watchLater == 1) {
                vm.removeMovieFromWatchLater(it)
                vm.loadWatchLaterMovies()
            } else {
                vm.addMovieToWatchLater(it)
                vm.loadWatchLaterMovies()
            }
        })
    }

    private fun setVMStateSubscriptions() {
        vm.state.observe(this, Observer {
            when (it) {
                BaseViewModel.Status.LOADING -> {
                    showLoading(true)
                }
                BaseViewModel.Status.SUCCESS -> {
                    showLoading(false)
                    vm.response?.let {
                        adapter.setMovies(ArrayList(it))
                    }
                }
                BaseViewModel.Status.MOVIE_ADDED_TO_WL -> {
                    Toast.makeText(context, getString(R.string.movie_added_to_wl), Toast.LENGTH_SHORT).show()
                }
                BaseViewModel.Status.MOVIE_REMOVED_FROM_WL -> {
                    Toast.makeText(context, getString(R.string.movie_removed_from_wl), Toast.LENGTH_SHORT).show()
                }
                BaseViewModel.Status.MOVIE_ADDED_TO_FAV -> {
                    Toast.makeText(context, getString(R.string.movie_added_to_fav), Toast.LENGTH_SHORT).show()
                }
                BaseViewModel.Status.MOVIE_REMOVED_FROM_FAV -> {
                    Toast.makeText(context, getString(R.string.movie_removed_from_fav), Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

}
