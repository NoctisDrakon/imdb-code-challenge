package com.example.imdbmovies.core.dagger.modules

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.example.imdbmovies.BuildConfig
import com.example.imdbmovies.core.api.API
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import javax.inject.Singleton

@Module
class ApiModule(val context: Context) {

    @Provides
    @Singleton
    fun provideApi(): API {
        val httpCache = Cache(context.cacheDir, CACHE_SIZE)

        val customHttpClient = OkHttpClient.Builder()
            .cache(httpCache)
            .addInterceptor { chain ->
                var request = chain.request()
                request = if (hasNetwork(context) == true) {
                    request.newBuilder().header("Cache-Control", "public, max-age=" + 5).build()
                } else {
                    request.newBuilder()
                        .header("Cache-Control", "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7).build()
                }
                chain.proceed(request)
            }
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(customHttpClient)
            .build()
        return retrofit.create(API::class.java)
    }

    companion object {
        const val CACHE_SIZE = (5 * 1024 * 1024).toLong() //5mb Cache

        fun hasNetwork(context: Context): Boolean? {
            var isConnected: Boolean? = false
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
            if (activeNetwork != null && activeNetwork.isConnected)
                isConnected = true
            return isConnected
        }
    }

}