package com.example.imdbmovies.core.base

import com.example.imdbmovies.core.api.API
import com.example.imdbmovies.core.dagger.Injector
import com.example.imdbmovies.core.db.IMDBDatabase
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import javax.inject.Inject

abstract class BaseRepository {

    protected val globalDisposable: CompositeDisposable = CompositeDisposable()

    @Inject
    lateinit var api: API

    @Inject
    lateinit var dataBase: IMDBDatabase

    init {
        Injector.component().inject(this)
    }

    fun addDisposable(disposable: Disposable) {
        globalDisposable.add(disposable)
    }
}