package com.example.imdbmovies.core.base

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewModel : ViewModel() {

    protected val globalDisposable: CompositeDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        globalDisposable.dispose()
    }

    fun addDisposable(disposable: Disposable) {
        globalDisposable.add(disposable)
    }

    /**
     * Different VM states shared across all the available VMs
     */
    enum class Status {
        LOADING, SUCCESS,
        MOVIE_ADDED_TO_FAV,
        MOVIE_REMOVED_FROM_FAV,
        MOVIE_ADDED_TO_WL,
        MOVIE_REMOVED_FROM_WL,
        NO_MOVIES_IN_DB,
        ERROR }
}