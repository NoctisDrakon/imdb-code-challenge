package com.example.imdbmovies.core.dagger.components

import com.example.imdbmovies.core.base.BaseFragment
import com.example.imdbmovies.core.base.BaseRepository
import com.example.imdbmovies.core.dagger.modules.ApiModule
import com.example.imdbmovies.core.dagger.modules.DatabaseModule
import com.example.imdbmovies.favorites.FavoritesViewModel
import com.example.imdbmovies.movies.MoviesRepository
import com.example.imdbmovies.movies.MoviesViewModel
import com.example.imdbmovies.watchLater.WatchLaterViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApiModule::class, DatabaseModule::class])
interface IMDBComponent {
    fun inject(baseFragment: BaseFragment)
    fun inject(baseRepository: BaseRepository)
    fun inject(moviesRepository: MoviesRepository)
    fun inject(moviesViewModel: MoviesViewModel)
    fun inject(favoritesViewModel: FavoritesViewModel)
    fun inject(watchLaterViewModel: WatchLaterViewModel)
}