package com.example.imdbmovies.core.base

import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.example.imdbmovies.core.dagger.Injector
import com.example.imdbmovies.core.dagger.components.DaggerIMDBComponent
import com.example.imdbmovies.extensions.visible
import com.example.imdbmovies.util.GlideFactory
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.loading_view.*
import javax.inject.Inject

abstract class BaseFragment : Fragment() {

    @Inject protected lateinit var glideFactory: GlideFactory
    private val globalDisposable = CompositeDisposable()

    init {
        Injector.component().inject(this)
    }

    protected fun showLoading(show: Boolean) {
        loading_view.visible = show
    }

    protected fun addDisposable(disposable: Disposable) {
        globalDisposable.add(disposable)
    }

    override fun onDestroy() {
        super.onDestroy()
        globalDisposable.dispose()
    }
}