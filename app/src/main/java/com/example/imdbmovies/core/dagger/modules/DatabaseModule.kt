package com.example.imdbmovies.core.dagger.modules

import android.app.Application
import com.example.imdbmovies.core.db.IMDBDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule(val application: Application) {

    @Provides
    @Singleton
    fun provideDataBase(): IMDBDatabase {
        IMDBDatabase.createInstance(application)
        return IMDBDatabase.getInstance()
    }

}