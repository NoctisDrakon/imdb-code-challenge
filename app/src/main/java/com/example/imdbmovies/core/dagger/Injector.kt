package com.example.imdbmovies.core.dagger

import com.example.imdbmovies.core.dagger.components.IMDBComponent

class Injector {

    companion object {

        @JvmStatic
        private lateinit var component: IMDBComponent

        @JvmStatic
        fun init(component: IMDBComponent) {
            this.component = component
        }

        @JvmStatic
        fun component() = component
    }

}