package com.example.imdbmovies.core.db

import androidx.room.*
import com.example.imdbmovies.movies.Movie
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
interface MovieDao {
    @Insert
    fun insertMovie(movie: Movie): Single<Long>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAllMovies(movies: List<Movie>): Single<List<Long>>

    @Update
    fun updateMovie(movie: Movie)

    @Delete
    fun deleteMovie(movie: Movie)

    @Query("DELETE FROM table_movies")
    fun deleteAllMovies() : Maybe<Unit>

    @Query("SELECT * FROM table_movies")
    fun getAllMovies(): Maybe<List<Movie>>

    @Query("SELECT * FROM table_movies where favorite = 1")
    fun getFavorites(): Maybe<List<Movie>>

    @Query("UPDATE table_movies SET favorite = 1 where id = :movieId")
    fun addMovieToFavorites(movieId: Int): Maybe<Unit>

    @Query("UPDATE table_movies SET favorite = 0 where id = :movieId")
    fun removeMovieFromFavorites(movieId: Int): Maybe<Unit>

    @Query("SELECT * FROM table_movies where watchLater = 1")
    fun getWatchLater(): Maybe<List<Movie>>

    @Query("UPDATE table_movies SET watchLater = 1 where id = :movieId")
    fun addMovieToWatchLater(movieId: Int): Maybe<Unit>

    @Query("UPDATE table_movies SET watchLater = 0 where id = :movieId")
    fun removeMovieFromWatchLater(movieId: Int): Maybe<Unit>
}