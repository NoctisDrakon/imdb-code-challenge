package com.example.imdbmovies.core.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.imdbmovies.BuildConfig
import com.example.imdbmovies.movies.Movie
import timber.log.Timber

@Database(entities = [Movie::class], version = BuildConfig.DATABASE_VERSION)
abstract class IMDBDatabase : RoomDatabase() {

    companion object {
        @JvmStatic
        private lateinit var instance: IMDBDatabase

        @JvmStatic
        @Synchronized
        fun createInstance(context: Context) {
            instance = Room.databaseBuilder(
                context.applicationContext,
                IMDBDatabase::class.java, BuildConfig.DATABASE_NAME
            )
                .fallbackToDestructiveMigration()
                .addCallback(callback)
                .build()
        }

        @JvmStatic
        @Synchronized
        fun getInstance() = instance

        val callback = object : RoomDatabase.Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                Timber.d("Database created!")
            }

            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)
                Timber.d("Database open!")
            }
        }

    }

    abstract fun movieDao() : MovieDao
}