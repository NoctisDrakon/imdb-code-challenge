package com.example.imdbmovies.core.api

import com.example.imdbmovies.BuildConfig
import com.example.imdbmovies.movies.Movie
import com.example.imdbmovies.movies.Response
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface API {

    @GET("movie/popular")
    fun getMovies(
        @Query("api_key") apiKey: String = BuildConfig.IMDB_KEY,
        @Query("language") language: String,
        @Query("page") page: Int
    ): Single<Response>

    @GET("search/movie")
    fun searchMovies(
        @Query("api_key") apiKey: String = BuildConfig.IMDB_KEY,
        @Query("query") query: String,
        @Query("page") page: Int
    ): Single<Response>

}