# IMDB code challenge

Welcome to the repository of my IMDB Code challenge app. In this Readme file you'll find a detailed explanation of my app and its functionalities.

Main screen
--------

The main screen of this app, will present you a list of the most popular movies of this year fetched from [The Movie Database][1]. You can also search for a term using the upper search box and you will get a list of movies from this year that matches the term or terms you searched for.

Either using the default movies list or searching your own terms, you also have the option to sort the results using the three-dot button located next to the search box. You can order the results by Descending name or Ascending date.

Wanna see more of a movie? Just tap on the movie poster and you'll be taken to the details screen where you'll see more detailed information of the movie.

Favorites
--------
Did you find a movie that you already watched and loved it? Say no more! Add it to your personal favorites list! Just click the start icon located below the movie poster and your movie will be added to your favorites list. Favorites can be consulted clicking on the **Favorites** tab located at the bottom of the screen. You can add movies to your favorites either from the default movies list or the search results, and you can always change your mind by pressing again the star button (from either screen) and the movie you previously saved, will be removed from your list. Don't worry, everybody has the right to change their mind ;)

Watch later
--------
Oh! Did you find that new movie that your friends were talking about how cool it was? Then don't forget to watch it later! You can add the movie to you **Watch later** personal list by clicking the three-dot button located above the movie poster and clicking on **Watch later** and it will be immediatly saved to your personal Watch Later collection. Wait... what? do they say the movie is not that good after all? Not an issue! You can always remove it by clicking the three-dots button again and clicking **Remove from watch later** from either the movies screen or from your watch later screen. After all, why spending time watching a new movie when you can watch Back to the future for the 1000th time?

No internet? Not an issue!
--------
We know that not all of us can afford one of those fancy unlimited data plans (The developers of this app don't even have a decent phone for themselves). That's why you'll never have to worry if you don't have an internet connection out there and you want to check you **Watch Later** movies just to be sure you're purchasing the right entrance to the movie theater, or if you want to consult your **Favorites** in case you need a good conversation topic for your date tonight. Even if you want to check the movies you searched for the last time, that's not a problem for us!

**Favorites** and **Watch later** lists are available 100% while offline, Also, every movie you have ever looked at in our app, is internally stored both in cache and in memory. As soon as you open the app, we will try to fetch the movies you already looked at and present them in first place. If you scroll down and you reach the limit of movies we have stored in the internal database, the app will automatically start fetching movies from the external API. Even if you want to search for a term and you don't have internet connection, if you have previously looked for that term, we will fetch the previous results you obtained that we have stored in cache. For example, let's suppose that you looked for **The lion king** today earlier while you were at home connected to your WiFi network and you obtained a couple of results, and later while on the bus and without an internet connection, you open the app and search for **The lion king** again, you will get exactly the same results you got earlier, and you wil be able to see the movie details, add the movie to your favorites, your watch later list, etc. Just like if it was a realtime search. 

Adaptability to different devices
--------
Don't worry if you ever get a bigger device, such as an awesome tablet. This app is designed to offer you a nice and smooth view depending on the type of device you are using it. For example, if you are using a normal phone, you'll see two movie tiles per row, but if you're using a tablet, then you'll see 4 movie tiles per row. This will offer you an awesome user experience while using this app on different devices.

Credits
--------
This app was developed by Gabino Rutiaga as part of a code challenge for **BlueCoding**.


[1]: https://www.themoviedb.org

